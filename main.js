//SERVICE WORKER
if('serviceWorker' in navigator){
    console.log('Puedes usar los ServiceWorker en tu navegador');

    navigator.serviceWorker.register('./sw.js')
                            .then(res => console.log('service worker cargado', res))
                            .catch(err => console.log('ServiceWorker no se ha podido registrar', err));
}else{
    console.log('No puedes');
}



//SCROLL SUAVIZADO
$(document).ready(function(){
    $("#menu a").click(function(e){
        e.preventDefault();

        $("html, body").animate({
            scrollTop: $((this).attr('href')).offset().top
        })

        return false;
    });
});