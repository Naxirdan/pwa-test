'use strict'
//Assignar nombre y version de cache

const CACHE_NAME = 'v1_pwa_test';

//Ficheros a poner en la cache de la app
var urlsToCache = [
    './',
    './img/favicon.png',
    './img/favicon-16.png',
    './img/favicon-32.png',
    './img/favicon-64.png',
    './img/favicon-96.png',
    './img/favicon-128.png',
    './img/favicon-192.png',
    './img/favicon-256.png',
    './img/favicon-384.png',
    './img/favicon-512.png',
    './img/favicon-1024.png',
    './img/1.png',
    './img/2.png',
    './img/3.png',
    './img/4.png',
    './img/5.png',
    './img/6.png',
    './img/instagram.png',
    './img/twitter.png',
    './img/facebook.png',
];

//Evento de instalacion
//Instalacion del service worker y guardar en cache recursos estáticos.
self.addEventListener('install', e => {
    e.waitUntil(
        caches.open(CACHE_NAME)
                    .then(cache => {
                        return cache.addAll(urlsToCache)
                            .then(()=>{
                                self.skipWaiting();
                            });
                    })
                    .catch( err => {console.log('No se ha registrado la cache', err)})
    );
});

//Evento de activacion de la app
//Que la app funcione sin internet
self.addEventListener('activate', e => {
    const cacheWhiteList = [CACHE_NAME];

    e.waitUntil(
        caches.keys()
                .then(cacheNames => {
                    return Promise.all(
                        cacheNames.map(cacheName => {

                            if(cacheWhiteList.indexOf(cacheName) === -1){
                                //Borrar elementos que no se necesitan
                                return caches.delete(cacheName);
                            }
                        })
                    );
                })
                .then(() => {
                    //Activar cache
                    self.clients.claim();
                })
    );
});

//Evento Fetch
self.addEventListener('fetch', e => {
    e.respondWith(
        caches.match(e.request)
                    .then( res => {
                        if(res){
                            //devuelvo datos desde cache
                            return res;
                        }
                        return fetch(e.request);
                    })
    );
});